console.log("Soal no 1") 

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word, second, third, fourth, fifth, sixth, seventh) 

console.log("===================================================================") 

console.log("Soal no 2") 

var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var exampleThirdWord = sentence[5]+sentence[6]+sentence[7]+sentence[8] + sentence[9];
var exampleFourthWord = sentence[11] + sentence[12]  ; 
var exampleFifthWord = sentence[14] + sentence[15]  ; 
var exampleSixthWord = sentence[17]+sentence[18]+sentence[19]+sentence[20] + sentence[21]; // lakukan sendiri 
var exampleSeventhWord = sentence[23]+sentence[24]+sentence[25]+sentence[26] + sentence[27]; // lakukan sendiri 
var exampleEighthWord= sentence[29]+sentence[30]+sentence[31]+sentence[32] + sentence[33]+sentence[34]+sentence[35]+sentence[36] + sentence[37]+ sentence[38]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + exampleThirdWord); 
console.log('Fourth Word: ' + exampleFourthWord); 
console.log('Fifth Word: ' + exampleFifthWord); 
console.log('Sixth Word: ' + exampleSixthWord); 
console.log('Seventh Word: ' + exampleSeventhWord); 
console.log('Eighth Word: ' + exampleEighthWord)

console.log("===================================================================") 

console.log("Soal no 3") 

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);// do your own! 
var thirdWord2= sentence2.substring(15, 17); // do your own! 
var fourthWord2= sentence2.substring(18, 20); // do your own! 
var fifthWord2= sentence2.substring(21, 25); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log("===================================================================") 

console.log("Soal no 4") 

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var exampleSecondWord3 = sentence3.substring(4, 14);// do your own! 
var exampleThirdWord3= sentence3.substring(15, 17); // do your own! 
var exampleFourthWord3= sentence3.substring(18, 20); // do your own! 
var exampleFifthWord3= sentence3.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length 
var seconndWordLength = exampleSecondWord3.length
var thirdWordLength = exampleThirdWord3.length
var fourthWordLength = exampleFourthWord3.length
var fifthWordLength = exampleFifthWord3.length
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + exampleSecondWord3 + ', with length: ' + seconndWordLength); 
console.log('Third Word: ' + exampleThirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + exampleFourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + exampleFifthWord3 + ', with length: ' + fifthWordLength); 