console.log('Soal No.1 Looping(While)')

    console.log('Looping Pertama')
for(var deret = 2; deret <= 20; deret += 2) {    
    console.log(deret + ' - I love coding' );
  }

console.log('Looping Kedua')
for (var deret = 20; deret >0; deret -= 2){
    console.log(deret + ' - I will become a mobile developer');
}
   
console.log ("=======================================================================================")
console.log('Soal No.2 Looping(for)')
console.log('Output')
for (var x = 0; x <= 20; x++){
    if (x % 2 != 0 && x % 3 != 0){
        console.log(x + " - Santai");
    }
    if (x % 2 == 0 && x>0){
        console.log(x + " - Berkualitas");
    }
    if (x % 3 == 0 && x % 2 != 0 ){
        console.log(x + " - I Love Coding");
    }
}


console.log ("=======================================================================================")
console.log('Soal No.3 Membuat Persegi Panjang(##)')
var output = '';
for (var x=0; x<4; x++){    
    for (var y=0; y <= 8; y++){
            output += '#';
        }
        output += '\n';
}
console.log(output);

console.log ("=======================================================================================")
console.log('Soal No.4 Membuat Tangga')
var output = '';
for (var x=0; x<7; x++){    
    for (var y=0; y <= x; y++){
        output += '#';
        }
        output += '\n';
}
console.log(output);

console.log ("=======================================================================================")
console.log('Soal No.5 Membuat Papan Catur')
var output = "";
for (var x = 0; x < 4; x++) {
  for (var y = 0; y < 4; y++) {
    output += ` `
    output += `#`;
    
  }
  output += "\n";
  for (var z = 0; z < 4; z++) {
    output += `#`;
    output += ` `
  }
  output += "\n";
}
console.log(output);


