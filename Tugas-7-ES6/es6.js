console.log("Soal no 1")
const golden = goldenFunction = () =>{
    console.log("this is golden!!")
}
golden()

console.log("===============================")
console.log("Soal no 2")
const newFunction = literal = (firstName, lastName) =>{
    return {firstName,lastName,
     fullName (){
        console.log(firstName + " "+lastName)
        return  
        }
    }
}
  newFunction("William", "Imoh").fullName()

console.log("===============================")
console.log("Soal no 3")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName,lastName,destination,occupation} = newObject
console.log(firstName, lastName, destination, occupation)

console.log("===============================")
console.log("Soal no 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [[...west,...east]]
console.log(combined) 

console.log("===============================")
console.log("Soal no 5")
const planet = 'earth'
const view = 'glass'
 
const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
console.log(before) // Zell Liew, unaffiliated